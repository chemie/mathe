.. _articles:

*******
Artikel
*******

Auf dieser Seite findest Du erklärende Artikel zu grundlegenden 
mathematischen Zusammenhängen und deren Anwendung zur Beschreibung und
Erklärung von Phänomenen der physikalischen und theoretischen Chemie sowie
Physik.

Folgende Themengebiete stehen zur Auswahl:

.. toctree::
    :maxdepth: 1

    math/index
    physchem/index
