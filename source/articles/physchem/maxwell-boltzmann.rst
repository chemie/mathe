.. _maxwell-boltzmann:

********************************************
Maxwell-Boltzmann-Geschwindigkeitsverteilung
********************************************

Betrachtet man ein ideales Gas in einem Raum, gibt es zwei wichtige 
Beobachtungen: Die einzige Wechselwirkung der Teilchen untereinander und 
mit der Wand sind harte elastische Stöße. Weil alle Stöße elastisch sind, 
bleibt die Energie des Systems erhalten. Die Gesamtenergie in unserem Modell
ist ausschließlich kinetischer Natur (d.h. es gibt keine potentielle Energie
im Raum und zwischen den Teilchen). Darüber hinaus ändern sich die 
Geschwindigkeiten der Teilchen durch Stöße permanent. In der folgenden 
Simulation starten alle Teilchen mit der gleichen Geschwindigkeit in eine 
zufällige Richtung. Durch Stöße mit anderen Teilchen und der Wand werden
Impulse ausgetauscht und die Geschwindigkeiten der Teilchen ändern sich 
ständig. Im Histogramm wird zu jedem Zeitpunkt die aktuelle Verteilung der 
Geschwindigkeiten abgebildet. Mit der Zeit scheint die 
Geschwindigkeitsverteilung eine bestimmte Form anzunehmen:

.. raw:: html
    :file: ../../_static/animations/ideal-gas.html

Offenbar sind in diesem idealen Gas einige Geschwindigkeiten 
wahrscheinlicher als andere. Die mathematische Beschreibung dieses Phänomens
ist die Maxwell-Boltzmann-Geschwindigkeitsverteilung. Sie gibt an, wie hoch 
die Wahrscheinlichkeit ist, dass ein Teilchen bei einer festen Temperatur 
:math:`T` die Geschwindigkeit :math:`v` hat. Denkt man an ein Gas, das aus 
vielen Teilchen besteht, so kann die Verteilungsfunktion auch als Anteil der
Teilchen mit der Geschwindigkeit :math:`v` interpretiert werden.

Zusammensetzung der Verteilungsfunktion
=======================================

Auf den ersten Blick wirkt die Formel sehr kompliziert:

.. math::
  f(v) = 
  \underbrace{\left(\frac{m}{2\pi kT}\right)^{\frac32}}_\text{
  Normierungsfaktor} \cdot
  \underbrace{4\pi v^2}_\text{Gewichtungsfaktor} \cdot
  \underbrace{e^{-\frac{mv^2}{2kT}}}_\text{Boltzmann-Faktor}

Zerlegen wir diese Formel einmal in ihre einzelnen Bestandteile:

Der Boltzmann-Faktor
--------------------

Starten wir mit dem Exponentialterm: Dieser ist der so genannte 
Boltzmann-Faktor, der in seiner allgemeinen Form die Gestalt

.. math::
  e^{-\beta E} = e^{-\frac{E}{kT}}

hat. Er ist proportional zu der Wahrscheinlichkeit, dass sich ein Teilchen
in einem Zustand der Energie :math:`E` befindet. Da es im idealen Gas keine
potentielle Energie gibt, entspricht die Energie :math:`E` der kinetischen
Energie :math:`\frac12 mv^2`. Der Boltzmann-Faktor gibt hier also ein Maß
dafür, wie groß die Wahrscheinlichkeit ist, dass ein Teilchen im idealen
Gas eine bestimmte kinetische Energie und damit eine bestimmte
Geschwindigkeit hat.

Das statistische Gewicht
------------------------

Diese Geschwindigkeit kann jedoch in jede Richtung im Raum gerichtet sein, 
da der Boltzmann-Faktor nur eine Aussage über den Betrag der Geschwindigkeit
trifft. Betrachten wir nun ein einzelnes Teilchen zu einem festen Zeitpunkt.
Wenn wir den Betrag der Geschwindigkeit kennen und berücksichtigen, dass 
sich das Teilchen theoretisch in alle Richtungen bewegen kann, müssen wir 
den Boltzmann-Faktor mit dem Gewichtungsfaktor :math:`4\pi v^2` 
multiplizieren. Warum? Am einfachsten ist eine geometrische Interpretation: 
hat das Teilchen zu einem festen Zeitpunkt die Geschwindigkeit :math:`v`,
so decken alle möglichen von ihm ausgehenden Geschwindigkeitsvektoren die 
Oberfläche einer Kugel mit Radius :math:`v` ab. Und was ist die Oberfläche
einer Kugel mit Radius :math:`v`? Richtig, :math:`4\pi v^2`.

Der Normierungsfaktor
---------------------

Zuletzt steht die Forderung, dass das Teilchen zu jedem Zeitpunkt irgendeine
Geschwindigkeit haben muss. In anderen Worten muss die Wahrscheinlichkeit, 
dass die Geschwindigkeit des Teilchens zwischen :math:`0` und :math:`\infty`
liegt, zu jedem Zeitpunkt 100% betragen. Mathematisch gesprochen bedeutet 
das:

.. math::
  \int_0^\infty f(v) \, \mathrm{d}v = 1

Aus dieser Forderung ergibt sich der Normierungsfaktor 
:math:`\left(\dfrac{m}{2\pi kT}\right)^{\frac32}`.

Das Maximum ist temperaturabhängig
==================================

Das Maximum der Verteilung kann durch Nullsetzen der ersten Ableitung 
erhalten werden (welche Regeln müssen dafür angewandt werden?). Daraus 
ergibt sich, dass das Maximum der Verteilung temperaturabhängig ist:

.. plot:: _static/pyplot/maxwell-boltzmann-temp.py
    :width: 100%

Entspricht die wahrscheinlichste Geschwindigkeit dem Erwartungswert 
:math:`\left< v \right>` der Verteilung? Dieser kann folgendermaßen 
berechnet werden:

.. math::
  \left< v \right> = \int_0^\infty v f(v) \, \mathrm{d}v

*Kleiner Einschub*: Die Berechnung dieser Erwartungswerte spielt eine 
wichtige Rolle in der kinetischen Gastheorie. Dort wird zur Berechnung des 
Drucks in einem mikroskopischen System der Erwartungswert des 
Geschwindigkeitsquadrats, also :math:`\left< v^2 \right>` benötigt. Ist 
dieser identisch zu :math:`\left< v \right>^2`?
