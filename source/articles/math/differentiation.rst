.. _differentiation:

***************
Differentiation
***************

.. toctree::
    :caption: Inhaltsverzeichnis
    :maxdepth: 2

.. abstract::

    Die *Ableitung* einer Funktion gibt die Tangentensteigung an jedem Punkt
    an. Die Steigung der Tangente gibt ein Maß für die Änderungsrate der 
    entsprechenden Funktion. Sie kann durch den *Differenzenquotienten*

    .. math::
        \frac{\Delta f(x)}{\Delta x} = \frac{f(x+\Delta x)-f(x)}{\Delta x}

    approximiert werden. Macht man :math:`h` immer kleiner, so kommt man
    schlussendlich von der Sekante (zwei Schnittpunkte mit der Funktion)
    zur Tangente. Der Differenzenquotient wird dabei in den so genannten
    *Differentialquotienten* überführt, der der Ableitung der Funktion
    entspricht:

    .. math::
        \lim_{\Delta x \to 0} \frac{f(x+\Delta x)-f(x)}{\Delta x} = f'(x) = 
        \frac{\mathrm{d}}{\mathrm{d}x} f(x) = 
        \frac{\mathrm{d}f(x)}{\mathrm{d}x}

    Wichtige bzw. häufig auftretende Ableitungen sind:

    .. csv-table::
        :header: ":math:`f(x)`", ":math:`f'(x)`", "Anmerkung"

        ":math:`k`", ":math:`0`", ":math:`k \in \mathbb{R}`"
        ":math:`x`", ":math:`1`"
        ":math:`x^n`", ":math:`nx^{n-1}`", ":math:`n \in \mathbb{R}`"
        ":math:`e^{\alpha x}`", ":math:`\alpha e^{\alpha x}`"
        ":math:`e^{x^2}`", ":math:`2x e^{x^2}`"
        ":math:`\ln(x)`", ":math:`\frac1x`"
        ":math:`\sin(x)`", ":math:`\cos(x)`"
        ":math:`\cos(x)`", ":math:`-\sin(x)`"
        ":math:`\tan(x)`", ":math:`\frac{1}{\cos^2(x)}`"

Ableitungen – Einführung und Motivation
=======================================

Der Differentialquotient
========================

Nun da wir wissen, dass sich die Änderungsrate einer Funktion in Abhängigkeit
ihrer Variablen durch die Steigung der Tangente ausdrücken lässt, stellt sich
die Frage: wie kann man diese Steigung berechnen?

Ein sinnvoller Ansatz ist, diese Steigung zu approximieren. Die Tangente wird 
durch eine Sekante angenähert, die die Funktion nicht nur in einem, sondern 
in zwei Punkten schneidet. Die Steigung dieser Sekante ist durch den so 
genannten *Differenzenquotienten* gegeben, der bereits aus der Schule bekannt
ist:

.. math::
    \frac{\Delta f(x)}{\Delta x} = \frac{f(x + \Delta x) - f(x)}{\Delta x}

Diese Näherung wird umso genauer, je kleiner man :math:`\Delta x` macht, wie
das folgende Beispiel zeigt:

.. raw:: html
    :file: ../../_static/animations/tangent.html

Macht man :math:`\Delta x` *infinitesimal* klein, erhält man die exate
Tangentensteigung und somit die exakte Ableitung. Diese ist dann durch einen
Spezialfall des Differenzenquotienten – den so genannten 
*Differentialquotienten* – gegeben:

.. math::
    f'(x) = \frac{\mathrm{d}}{\mathrm{d}x} f(x) = 
    \frac{\mathrm{d}f(x)}{\mathrm{d}x} =
    \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x}

Doch lässt sich der Differentialquotient überhaupt exakt berechnen? 
Schließlich ist es allgemein bekannt, dass man nicht durch Null teilen kann,
weshalb die Berechnung des Differentialquotienten auf den ersten Blick
unmöglich erscheint. Heißt das, wir können Ableitungen nur approximieren?

Versuchen wir einmal am Beispiel :math:`f(x) = x^n`, den Differentialquotienten
so umzuformulieren, dass sich :math:`\Delta x` aus dem Nenner wegkürzen lässt,
wobei wir uns zunächst auf natürliche Zahlen :math:`n` beschränken:

.. math::
    f'(x) &= \lim_{\Delta x \to 0} \frac{f(x + \Delta x) - f(x)}{\Delta x} \\
    &= \lim_{\Delta x \to 0} \frac{(x+\Delta x)^n - x^n}{\Delta x} \\

.. collapsible:: **Einschub:** Polynome und Binomialkoeffizienten

    Ein Ausdruck wie :math:`(a+b)^n` mit :math:`n \in \mathbb{N}` ergibt ein
    Polynom in :math:`a` und :math:`b`. Die Koeffizienten der einzelnen 
    Summanden lassen sich durch die so genannten *Binomialkoeffizienten*
    ausdrücken:

    .. math::
        (a+b)^n = \sum_{k=0}^n {n \choose k} a^{n-k}b^k

    Die Binomialkoeffizienten :math:`n \choose k` (gesprochen ":math:`n` über
    :math:`k`") besitzen im Allgemeinen die Form

    .. math::
        {n \choose k} = \frac{n!}{k! \, (n-k)!}.

    :math:`n!` (gesprochen ":math:`n` Fakultät") steht dabei für das Produkt 
    aller natürlichen Zahlen bis :math:`n` (ausgenommen 0):

    .. math::
        n! = 1 \cdot 2 \cdot 3 \cdot \dotsc \cdot (n-1) \cdot n

    Diese Formeln wirken zugegebenermaßen ziemlich abstrakt und aus der Luft
    gegriffen, also verdeutlichen wir sie einmal an ein paar Beispielen:

    **Beispiel 1:** :math:`(a+b)^2`

    .. math::
        (a+b)^2 &= \sum_{k=0}^2 {2 \choose k} a^{2-k}b^k \\
        &= {2 \choose 0} a^2 + {2 \choose 1} ab + {2 \choose 2} b^2 \\
        &= \frac{2!}{0!\,2!} a^2 + \frac{2!}{1!\,1!} ab + \frac{2!}{2!\,0!} 
        b^2 \\
        &= \frac22 a^2 + \frac21 ab + \frac22 b^2 \\
        &= a^2 + 2ab + b^2

    Dieses Ergebnis sollte Dir von der ersten binomischen Formel bekannt sein.

    **Beispiel 2:** :math:`(a+b)^3`

    .. math::
        (a+b)^3 &= \sum_{k=0}^3 {3 \choose k} a^{3-k}b^k \\
        &= {3 \choose 0} a^3 + {3 \choose 1} a^2b + {3 \choose 2} ab^2
        + {3 \choose 3} b^3 \\
        &= \frac{3!}{0!\,3!} a^3 + \frac{3!}{1!\,2!} a^2b + \frac{3!}{2!\,1!}
        ab^2 + \frac{3!}{3!\,0!} b^3 \\
        &= \frac66 a^3 + \frac62 a^2b + \frac62 ab^2 \frac66 b^3 \\
        &= a^3 + 3a^2b + 3ab^2 + b^3

    Bei genauerem Hinsehen fällt auf, dass diese Binomialkoeffizienten auch
    auf eine andere Weise erhalten werden können: die Koeffizienten eines
    Polynoms :math:`(a+b)^n` entsprechen nämlich der :math:`(n+1)`-ten Zeile
    des Pascal'schen Dreiecks!

    .. math::
        :nowrap:

        \begin{gather}
            1 \\
            1 \qquad 1 \\
            1 \qquad 2 \qquad 1 \\
            1 \qquad 3 \qquad 3 \qquad 1 \\
            1 \qquad 4 \qquad 6 \qquad 4 \qquad 1 \\
            \vdots
        \end{gather}

    Mit diesem Wissen zur Ausformulierung von Polynomen können wir uns nun
    wieder der Lösung des Differentialquotienten widmen.

.. math::
    \Rightarrow f'(x) &= \lim_{\Delta x \to 0} \frac{\sum_{k=0}^n 
    {n \choose k} x^{n-k}\Delta x^k - x^n}{\Delta x} \\
    &= \lim_{\Delta x \to 0} \frac{{n \choose 0} x^n + {n \choose 1} 
    x^{n-1}\Delta x + \dotsb + {n \choose n-1} x\Delta x^{n-1} + {n \choose n}
    \Delta x^n - x^n}{\Delta x} \\
    &= \lim_{\Delta x \to 0} \frac{{n \choose 1} x^{n-1}\Delta x + \dotsb +
    {n \choose n-1} x\Delta x^{n-1} + {n \choose n} \Delta x^n}{\Delta x}

Im Zähler lässt sich nun :math:`\Delta x` ausklammern und anschließend
wegkürzen:

.. math::
    \Rightarrow f'(x) &= \lim_{\Delta x \to 0} \frac{\Delta x \left[
    {n \choose 1} x^{n-1} + {n \choose 2} x^{n-2}\Delta x + \dotsb + 
    {n \choose n-1} x\Delta x^{n-2} + {n \choose n} \Delta 
    x^{n-1}\right]}{\Delta x} \\
    &= \lim_{\Delta x \to 0} \left[{n \choose 1} x^{n-1} + {n \choose 2} 
    x^{n-2}\Delta x + \dotsb + {n \choose n-1} x\Delta x^{n-2} + {n \choose n}
    \Delta x^{n-1}\right]

Bis auf den ersten Term besitzt jeder Summand mindestens einen Faktor
:math:`\Delta x`, sodass all diese Terme beim Ausführen des Limes wegfallen
und nur noch der erste Term stehen bleibt. Es folgt:

.. math::
    f'(x) = {n \choose 1} x^{n-1} = nx^{n-1}
