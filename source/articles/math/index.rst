.. _math:

**********
Mathematik
**********

Auf dieser Seite findest Du Artikel zu den wichtigsten mathematischen
Grundlagen, die Du in Deinem Studium brauchen wirst.

.. toctree::
    :maxdepth: 1

    differentiation
    integration
