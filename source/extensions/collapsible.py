# -*- coding: utf-8 -*-

""" Display collapsible text blocks in HTML output.
Will be rendered as a `paragraph` element in LaTeX output.

Usage:
------
.. collapsible:: heading
    :nopdf:

    Collapsible text.

Arguments:
----------
heading (optional):
    Heading of the collapsible text block. Block expands when heading
    is clicked. Can include math. "Anzeigen" / "Verbergen" if no heading
    is provided.

Options:
--------
nopdf (optional):
    Use this flag if the content of the collapsible should not be included
    in LaTeX/PDF output.

This directive takes content."""

from docutils import nodes
from docutils.parsers.rst import directives, Directive
from sphinx.util.osutil import relative_uri

class Collapsible(nodes.General, nodes.Element):
    """ Node class for collapsibles. """

class CollapsibleDirective(Directive):
    """ Implements the `collapsible` directive. """

    optional_arguments = 1
    required_arguments = 0
    final_argument_whitespace = True
    has_content = True
    option_spec = {"nopdf": directives.flag}

    def run(self):
        self.assert_has_content()

        env = self.state.document.settings.env

        # create target node for insertion in the document
        target_id = "collapsible-{:d}".format(env.new_serialno("collapsible"))
        target_node = nodes.target("", "", ids=[target_id])

        # create node for collapsible
        node = Collapsible()

        node["target_id"] = target_id

        # set `include_in_latex` to false if `nopdf` flag is given
        if "nopdf" in self.options:
            node["include_in_latex"] = False
        else:
            node["include_in_latex"] = True

        # parse heading, set "Anzeigen" if none is given
        if self.arguments:
            heading = nodes.caption(u"{0}".format(self.arguments[0]),
                                    u"{0}".format(self.arguments[0]))
        else:
            heading = nodes.caption(u"Anzeigen", u"Anzeigen")
        self.state.nested_parse(heading, 0, node)

        # parse content
        self.state.nested_parse(self.content, self.content_offset, node)

        return [target_node, node]

def html_visit_collapsible(self, node):
    """ Start of collapsible in HTML. """
    self.body.append(self.starttag(node, "div", CLASS="collapsible"))
    self.body.append("<a href=\"javascript:void(0)\" " +
                     "class=\"collapsible-trigger\">")
    heading = node.children.pop(0)
    for child in heading:
        child.walkabout(self)
    self.body.append("</a>")
    self.body.append("<div class=\"collapsible-content\">")

def html_depart_collapsible(self, node):
    """ End of collapsible in HTML. """
    if "path" in node:
        self.body.append(
            "<a href=\"" +
            relative_uri(self.docnames[0] + ".html", node["path"] + ".html") +
            "\" class=\"abstract-link\">Mehr zum Thema</a>"
        )
    self.body.append("</div></div>")

def latex_visit_collapsible(self, node):
    """ Start of content in LaTeX (will just be rendered as text). """
    if node["include_in_latex"]:
        # implement collapsible as paragraph
        self.body.append("\\paragraph{")
        heading = node.children.pop(0)
        for child in heading:
            child.walkabout(self)
        self.body.append("}\n")
    else: raise nodes.SkipNode

def latex_depart_collapsible(self, node):
    """ End of content in LaTeX. """

def setup(app):
    """ Adds `Collapsible` node and `collapsible` directive. """
    app.add_node(
        Collapsible,
        html=(html_visit_collapsible, html_depart_collapsible),
        latex=(latex_visit_collapsible, latex_depart_collapsible)
    )
    app.add_directive("collapsible", CollapsibleDirective)
