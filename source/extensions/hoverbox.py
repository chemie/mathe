# -*- coding: utf-8 -*-

""" Display hoverboxes in HTML when mousing over text.

Usage:
------
This is some :hoverbox:`text <label>` in a paragraph. Add the
hoverbox content after the paragraph is finished.

.. hoverbox-content:: label

    Content of the hoverbox

Arguments:
----------
label (optional):
    Label of the hoverbox. The content will be assigned to each hoverbox
    with the same label.

Options:
--------
None.

This directive takes content."""

from docutils import nodes
from docutils.parsers.rst import Directive
from sphinx.util.nodes import explicit_title_re

class Hoverbox(nodes.General, nodes.Element):
    """ Node class for hoverbox. """
    pass

class HoverboxContent(nodes.General, nodes.Element):
    """ Node class for hoverbox content. """
    pass

def hoverbox_role(name, rawtext, text, lineno, inliner, options={},
                  content=[]):
    """Implements the `hoverbox` role which can be used with or
    without a label. If no label is used, hoverboxes are enumerated
    in order of their appearance (not recommended)."""
    # create new node
    node = Hoverbox()

    # add text to mouse over
    hoverbox_text = nodes.paragraph()

    # check for custom label ("text <label>")
    explicit = explicit_title_re.match(text)
    if explicit:
        target_id = "hoverbox-{0}".format(explicit.group(2))
        hoverbox_text.append(nodes.Text(explicit.group(1)))
    else:
        env = inliner.document.settings.env
        target_id = "hoverbox-{:d}".format(env.new_serialno("hoverbox"))
        hoverbox_text.append(nodes.Text(text))

    node["id"] = target_id

    node.append(hoverbox_text)

    return [node], []

class HoverboxContentDirective(Directive):
    """Implemets the `hoverbox-content` direcitve."""

    optional_arguments = 1
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True

    def run(self):
        env = self.state.document.settings.env

        # create new node
        node = HoverboxContent()

        # check for custom label, otherwise assign ID number
        if self.arguments:
            target_id = "hoverbox-{0}-content".format(self.arguments[0])
        else:
            target_id = "hoverbox-{:d}-content".format(env.new_serialno("hoverbox_content"))

        # create target node
        target_node = nodes.target("", "", ids=[target_id])
        node["id"] = target_id

        # parse content of the hoverbox
        self.state.nested_parse(self.content, self.content_offset, node)

        return [target_node, node]

def html_visit_hoverbox(self, node):
    """ Start of hoverbox in HTML. """
    self.body.append("<span class=\"hoverbox-text\" id=\"" + node["id"] + "\">")

def html_depart_hoverbox(self, node):
    """ End of hoverbox in HTML. """
    self.body.append("</span>")

def html_visit_hoverbox_content(self, node):
    """ Start of hoverbox content in HTML. """
    self.body.append("<div class=\"hoverbox-content\" id=\"" + node["id"] + "\">")

def html_depart_hoverbox_content(self, node):
    """ End of hoverbox content in HTML. """
    self.body.append("</div>")

def latex_visit_hoverbox(self, node):
    pass

def latex_depart_hoverbox(self, node):
    pass

def setup(app):
    """ Adds `Hoverbox` and `HoverboxContent` node, `hoverbox` role
    and `hoverbox-content` directive. """
    app.add_node(
        Hoverbox,
        html=(html_visit_hoverbox, html_depart_hoverbox),
        latex=(latex_visit_hoverbox, latex_depart_hoverbox)
    )
    app.add_node(
        HoverboxContent,
        html=(html_visit_hoverbox_content, html_depart_hoverbox_content),
        latex=(latex_visit_hoverbox, latex_depart_hoverbox)
    )
    app.add_role("hoverbox", hoverbox_role)
    app.add_directive("hoverbox-content", HoverboxContentDirective)
