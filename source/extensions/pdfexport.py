# -*- coding: utf-8 -*-

""" Enables export of the document to LaTeX/PDF.

Usage:
------
.. pdfexport::
    :title: Title
    :author: Max Mustermann
    :documentclass: article

Arguments:
----------
None.

Options:
--------
title:
    Title of the document (will be printed at the top of the PDF)
author:
    Author of the document, separate multiple authors with `\\and`
documentclass:
    One of `article`, `report`, `book`, `letter`, `manual`, `howto`

The directive does not take content."""

import os
from docutils import nodes
from docutils.parsers.rst import directives, Directive

# get base path
FILE_DIR = os.path.dirname(os.path.realpath("__file__"))
PDFFILES_PATH = os.path.join(FILE_DIR, "extensions/pdffiles.py")

class PDFExport(nodes.General, nodes.Element):
    """ Node class for pdfexport. """
    pass

class PDFExportDirective(Directive):
    """ Implements the `pdfexport` directive. """
    optional_arguments = 0
    required_arguments = 0
    has_content = False
    option_spec = {
        "title": directives.unchanged_required,
        "author": directives.unchanged_required,
        "documentclass": lambda arg: directives.choice(
            arg, ("article", "report", "book", "letter", "manual", "howto"))
    }

    def run(self):
        # create target node for insertion in the document
        target_id = "pdfexport"
        target_node = nodes.target("", "", ids=[target_id])

        # create node for pdfexport
        node = PDFExport()
        node["target_id"] = target_id

        # get path to the source document relative to root directory (without ending),
        # e.g. articles/math/differentiation
        node["startdocname"] = os.path.relpath(
            self.state.document.attributes["source"][:-4], FILE_DIR)
        # replace slashes in the path by "___", add ".tex" extension
        node["targetname"] = node["startdocname"].replace("/", "___") + ".tex"
        # set title
        node["title"] = u"{0}".format(self.options.get("title"))
        # set author
        node["author"] = u"{0}".format(self.options.get("author"))
        # set documentclass
        node["documentclass"] = self.options.get("documentclass")

        return [target_node, node]

def html_visit_collapsible(self, node):
    """ Start of pdfexport in HTML. """

    # read content of pdffiles.py
    with open(PDFFILES_PATH, "r", encoding="utf-8") as file:
        pdffiles = file.readlines()

        # add a tuple in the shape of
        # (source file, target file, title, author, documentclass)
        # to pdffiles.py
        pdffiles.insert(2, "(\"" + node["startdocname"] + "\", \"" +
                        node["targetname"] + "\", u\"" + node["title"] +
                        "\", u\"" + node["author"] + "\", \"" +
                        node["documentclass"] + "\"),\n")
        pdffiles = "".join(pdffiles)

    # write new pdffiles.py
    with open(PDFFILES_PATH, "w") as file:
        file.write(pdffiles)

    # add download link in HTML
    pdffile = node["startdocname"].split("/").pop() + ".pdf"
    self.body.append(
        self.starttag(
            node, "a", CLASS="pdfdownload", TARGET="_blank", HREF=pdffile
        )
    )
    self.body.append("Download als PDF")

def html_depart_collapsible(self, node):
    """ End of pdfexport in HTML. """
    self.body.append("</a>")

def latex_visit_collapsible(self, node):
    """ Nothing special to do for LaTeX. """
    pass

def latex_depart_collapsible(self, node):
    """ Nothing special to do for LaTeX. """
    pass

def setup(app):
    """ Add `PDFExport` node and `pdfexport` directive. """
    app.add_node(
        PDFExport,
        html=(html_visit_collapsible, html_depart_collapsible),
        latex=(latex_visit_collapsible, latex_depart_collapsible)
    )
    app.add_directive("pdfexport", PDFExportDirective)
