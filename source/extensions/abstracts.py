# -*- coding: utf-8 -*-

""" Loads article abstracts as collapsibles

Usage:
------

.. abstracts::

    articles/integration
    articles/maxwell-boltzmann
"""

import os
import re
import qrcode
from docutils import nodes
from docutils.parsers.rst import Directive
from docutils.statemachine import StringList
from sphinx.util.nodes import explicit_title_re
from collapsible import Collapsible

class Abstracts(nodes.General, nodes.Element):
    """ Node class for abstracts. """

class AbstractsDirective(Directive):
    """ Implements the `abstracts` directive. """

    optional_arguments = 0
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True

    def run(self):
        target_node = nodes.target("", "", ids=["abstracts"])

        # create node for abstracts
        node = Abstracts()
        node["target_id"] = "abstracts"

        for article in self.content:
            # skip empty lines
            if not article:
                continue

            # create collapsible for abstract
            subnode = Collapsible()

            # set LaTeX inclusion to false (only titles will be included)
            subnode["include_in_latex"] = False

            # check for explicit titles ("Title <path_to_article>")
            # and set path and title
            explicit = explicit_title_re.match(article)

            if explicit:
                subnode["path"] = explicit.group(2)
                rawtitle = explicit.group(1)
            else:
                subnode["path"] = article
                rawtitle = u"{0}".format(self.state.document.settings.env.titles[article])
                # remove <title> and </title>
                rawtitle = rawtitle[7:-8]

            self.state.nested_parse(nodes.caption(rawtitle, rawtitle), 0, subnode)

            # copy content from abstract
            with open(subnode["path"] + ".rst", "r") as file:
                lines = file.read().splitlines()

                # regular expression for abstract directive
                abstract_start_re = re.compile(r"\.\. abstract::")
                # regular expression for abstract end (i.e. non-indented text)
                abstract_end_re = re.compile(r"^[a-zA-Z0-9].*$")

                # find beginning of abstract directive
                for index, line in enumerate(lines):
                    if abstract_start_re.match(line):
                        # remove everything before beginning of the abstract
                        lines = lines[index+2:]
                        break
                # raise warning if no abstract can be found, i.e., when the
                # for loop terminates without break
                else: raise RuntimeWarning("No abstract in " + article)
                # find end of abstract directive (i.e., the first non-indented line)
                for index, line in enumerate(lines):
                    if not abstract_end_re.match(line):
                        continue
                    # remove everything after ending of abstract
                    lines = lines[:index-1]
                    break

            abstract_content = StringList()
            for index, line in enumerate(lines):
                line = re.sub(r"\n", r"", line)
                line = re.sub(r"^ *$", r"", line)
                abstract_content.append(line, "", index)

            self.state.nested_parse(abstract_content, 0, subnode)

            node.append(subnode)

        return [target_node, node]

def html_visit_abstracts(self, node):
    """ Start of abstracts in HTML. """
    self.body.append(self.starttag(node, "div", CLASS="section"))

def html_depart_abstracts(self, node):
    """ End of abstracts in HTML. """
    self.body.append("</div>")

def latex_visit_abstracts(self, node):
    """ Abstracts will be included as bullet lists containing the title
    of the article, a QR code and a clickable hyperlink. """

    # create aligned bullet list
    self.body.append("\n\\begin{tabular}{@{$\\bullet$~}ll}\n")

    # loop over abstracts in Abstratcs node
    for i, abstract in enumerate(node.children):
        # URI for the article
        article_uri = "http://ebert.cup.uni-muenchen.de/html/math/" + abstract["path"] + ".html"

        qr_code_file = "_static/images/qrcodes/" + abstract["path"].replace("/", "___") + ".png"

        # check if QR code already exists. If not, create it
        if not os.path.isfile(qr_code_file):
            qr_code = qrcode.make(article_uri)
            qr_code.save(qr_code_file)

        # add article title
        abstract.children[0].walkabout(self)

        # add QR code and clickable link
        self.body.append("\\!\\!: & " +
                         "\\raisebox{-.5\\height}{" +
                         "\\includegraphics[width=2cm]{" +
                         "../../" + qr_code_file[:-4] + "}}" +
                         " (\\href{" + article_uri + "}{Link})\n")

        # add LaTeX linebreak (`\\`) if not last abstract
        if not i == (len(node.children)-1):
            self.body.append(" \\\\")

def latex_depart_abstracts(self, node):
    """ End of abstracs in LaTeX """
    self.body.append("\\end{tabular}\n")

###############################################################################

""" Abstract for an article, will be rendered as collapsible.
Will not be included in PDF.

Usage:
------
.. abstract::

    Abstract text.

Arguments:
----------
None.

Options:
--------
None.

This directive takes content."""

class AbstractDirective(Directive):
    """ Implements the `abstract` directive. """

    optional_arguments = 0
    required_arguments = 0
    final_argument_whitespace = False
    has_content = True

    def run(self):
        target_id = "abstract"
        target_node = nodes.target("", "", ids=[target_id])

        # create node for abstract
        node = Collapsible()
        node["target_id"] = target_id

        node["include_in_latex"] = False

        # "Kurzfassung" as heading for the collapsible
        heading = nodes.caption(u"Zusammenfassung", u"Zusammenfassung")
        self.state.nested_parse(heading, 0, node)

        # parse content
        self.state.nested_parse(self.content, self.content_offset, node)

        return [target_node, node]

def setup(app):
    """ Adds `Abstracts` node, `abstract` directive and `abstracts` directive. """
    app.add_node(
        Abstracts,
        html=(html_visit_abstracts, html_depart_abstracts),
        latex=(latex_visit_abstracts, latex_depart_abstracts)
    )
    app.add_directive("abstract", AbstractDirective)
    app.add_directive("abstracts", AbstractsDirective)
