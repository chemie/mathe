$(document).ready(function() {
	$('.hoverbox-text').hover(
		function() {
			var id = $(this).attr('id') + '-content';
			pos_calc($(this));
			$('#' + id).css({
				'visibility': 'visible', 
				'opacity' : 1
			});
		},
		function() {
			var id = $(this).attr('id') + '-content';
			$('#' + id).css({
				'visibility': 'hidden',
				'opacity' : 0
			});
		}
	);
});

$(window).resize(function() {
	$('hoverbox-text').each(function() {
		pos_calc($(this));
	});
});

function pos_calc(hoverbox) {
	var id = hoverbox.attr('id') + '-content'

	var section = $('#' + id).closest('.section');
	var section_left = section.position().left;
	var section_right = section_left + 
		section.outerWidth();

	$('#' + id).css('max-width', 
		section.outerWidth() * 0.8 + 'px');

	var parent_id = id.substring(0, id.length - 8);
		
	var text_pos = hoverbox.position();
	var text_width = hoverbox.outerWidth();

	var self_height = $('#' + id).outerHeight();
	var self_width = $('#' + id).outerWidth();

	var pos = {
		'top': text_pos.top - self_height,
		'left': text_pos.left + text_width/2,
		'margin': self_width/2
	};

	if (pos.left - pos.margin < section_left) {
		pos.left = section_left;
		pos.margin = 0;
	} else if (pos.left +  pos.margin > section_right) {
		pos.left = section_right - self_width;
		pos.margin = 0;
	}

	$('#' + id).css({
		'top': pos.top+ 'px',
		'left': pos.left + 'px',
		'margin-left': -pos.margin + 'px',
		'--arrow-left': text_pos.left - pos.left + 
			pos.margin + text_width/2 + 'px'
	});
}
