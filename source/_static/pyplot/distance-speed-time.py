import matplotlib.pyplot as plt
import numpy as np

fig, (s_t, v_t) = plt.subplots(2, sharex = True)

v = 1

s = lambda t: v * t 

t_range_1 = np.arange(0, 3, 0.01)
t_range_2 = np.arange(3, 6, 0.01)
t_range_3 = np.arange(6, 10, 0.01)

s_t.set(ylabel = r'$s \, / \, \mathrm{m}$')
s_t.plot(t_range_1, s(t_range_1), color = 'black')
s_t.plot(t_range_2, s(t_range_2), color = 'red', lw = 2)
s_t.plot(t_range_3, s(t_range_3), color = 'black')
s_t.axis([0, 10, 0, 12])
s_t.annotate(r'$\Delta t$', xy = (4.5, 2.5), xytext = (4.5, 0.8),
        xycoords = 'data', ha = 'center', va = 'bottom', 
        arrowprops = dict(arrowstyle = '-[, lengthB=0.3, widthB=5.4'))
s_t.annotate(r'$\Delta s$', xy = (6.1, 4.5), xytext = (6.3, 4.5),
        xycoords = 'data', ha = 'left', va = 'center',
        arrowprops = dict(arrowstyle = '-[, lengthB=0.3, widthB=1.5'))

v_t.set(xlabel = r'$t \, / \, \mathrm{s}$', 
        ylabel = r'$v \, / \, \mathrm{m \, s^{-1}}$')
v_t.plot(t_range_1, [v] * len(t_range_1), color = 'black')
v_t.plot(t_range_2, [v] * len(t_range_2), color = 'red', lw = 2)
v_t.fill_between(t_range_2, [0] * len(t_range_2), [v] * len(t_range_2),
        color = 'salmon')
v_t.plot(t_range_3, [v] * len(t_range_3), color = 'black')
v_t.axis([0, 10, 0, 2])
v_t.annotate(r'$\Delta s = v \cdot \Delta t$', xy = (4.5, 0.5),
        xycoords = 'data', ha = 'center', va = 'center')

plt.show()
