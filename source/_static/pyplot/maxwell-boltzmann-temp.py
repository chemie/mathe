import numpy as np
import matplotlib.pyplot as plt

R = 8.3144
M = 4.0026e-3

def f(v, T):
    return 4*np.pi*np.sqrt((M/(2*np.pi*R*T))*(M/(2*np.pi*R*T))*(M/(2*np.pi*R*T)))*v*v*np.exp(-(M*v*v)/(2*R*T))

def vmax(T):
    return np.sqrt(2*R*T/M)

v = np.arange(1, 4000, 1)

plt.xlabel(r'$v\,/\,{\rm m\,s^{-1}}$')
plt.ylabel(r'$f(v)$')
plt.plot(v, f(v, 25), color = 'magenta', lw = 2, label = r'$25\,{\rm K}$')
plt.plot(v, f(v, 50), color = 'cyan', lw = 2, label = r'$50\,{\rm K}$')
plt.plot(v, f(v, 85), color = 'blue', lw = 2, label = r'$85\,{\rm K}$')
plt.plot(v, f(v, 150), color = 'lightgreen', lw = 2, label = r'$150\,{\rm K}$')
plt.plot(v, f(v, 300), color = 'red', lw = 2, label = r'$300\,{\rm K}$')
plt.plot(vmax(v), f(vmax(v), v), color = 'black', lw = 1)
plt.legend()
plt.axis([0, 4000, 0, 3.0e-3])
plt.show()
