.. _CC-BY-SA:

*******************
CC-BY-SA 4.0-Lizenz
*******************

Alle Inhalte auf dieser Seite stehen unter einer 
`CC-BY-SA 4.0-Lizenz 
<https://creativecommons.org/licenses/by-sa/4.0/deed.de>`_.

Das bedeutet, dass Du sämtliche Inhalte frei weiterverwenden darfst – auch
modifiziert – so lange Du Dich dabei an folgende Bedingungen hältst:

  * **Namensnennung:** Du musst uns als Quelle für die von dieser Seite
    verwendeten Inhalte nennen, dabei Änderungen Deinerseits deutlich machen
    und – wenn möglich – unsere Website verlinken. Die Namensnennung sollte im
    Stil neutral gehalten sein, sodass nicht der Eindruck entsteht, dass wir
    Dein Projekt durch unsere Inhalte in besonderer Weise fördern.

  * **Weitergabe unter gleichen Bedingungen:** Wenn Du Inhalte unserer Seite
    für Dein eigenes Projekt modifizierst oder direkt darauf aufbaust, musst
    Du dieses Projekt ebenfalls unter CC-BY-SA 4.0 oder eine kompatible Lizenz
    stellen.

  * **Keine weiteren Einschränkungen:** Du darfst bei der Lizensierung
    Deines eigenen Projekts keine weiteren Einschränkungen vornehmen, wenn 
    Du unsere Inhalte verwendest (z.B. Modifizierungen oder kommerzielle 
    Nutzung verbieten).

Du bist Dir unsicher, was die Weiterverwendung unserer Materialien und die
Lizensierung Deines Projekts betrifft? Kontaktiere uns! Unser oberstes Ziel
ist es, unsere Inhalte so vielen wie möglich frei zugänglich zu
machen – es findet sich also in jedem Fall eine Lösung.
