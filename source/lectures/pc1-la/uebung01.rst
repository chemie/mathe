.. _uebung01:

.. pdfexport::
    :title: \\LARGE \\textbf{Übungsblatt 1}
    :author: Vorlesung ``Physikalische Chemie I für Lehramt\'\'
    :documentclass: article

*************
Übungsblatt 1
*************

Thema Differential- und Integralrechnung
========================================

Grundlagen
----------

.. abstracts::

    articles/math/differentiation
    articles/math/integration

Lösen von Differentialgleichungen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Viele physikalische Gesetze sind allgemein in einer differentiellen Form
definiert, z.B. die Bewegungsgleichung 
:math:`\frac{\mathrm{d}s}{\mathrm{d}t} = v`. Hier haben Sie zwei 
miteinander gekoppelte Variablen, nämlich Ort :math:`s` und Zeit :math:`t`. 
Somit können Sie die Unterschiede zwischen zwei Zuständen bestimmen, z.B. 
den Ort :math:`s_2` zur Zeit :math:`t_2`, wenn Ort :math:`s_1` und Zeit 
:math:`t_1` an Zustand 1 bekannt sind. Dafür werden zunächst die Variablen 
separiert (alle Abhängigkeiten von :math:`s` und :math:`\mathrm{d}s` auf 
eine Seite der Gleichung gebracht, während alle Abhängigkeiten von 
:math:`t` und :math:`\mathrm{d}t` auf die andere Seite kommen) und dann 
beide Seiten integriert. Die Bedingungen der Aufgabe definieren die 
jeweiligen Integrationsgrenzen.

| Variablentrennung: :math:`\frac{\mathrm{d}s}{\mathrm{d}t} = v \quad \to \quad \mathrm{d}s = v \mathrm{d}t`
| Integrieren: :math:`\int_{s_1}^{s_2} \,\mathrm{d}s = \int_{t_1}^{t_2} v\,\mathrm{d}t = v \int_{t_1}^{t_2} \,\mathrm{d}t \quad \to \quad [s]_{s_1}^{s_2} = v \cdot [t]_{t_1}^{t_2} \quad \to \quad s_2 - s_1 = v \cdot (t_2 - t_1)`
| Auflösen nach gesuchter Variable: :math:`s_2 = v \cdot (t_2 - t_1) + s_1`
| Frage: Was wurde implizit für :math:`v` angenommen?

Aufgaben
--------

1. Differentiation & Integration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

a) :math:`\int_{x_1}^{x_2}\,\mathrm{d}x`
b) | :math:`\int_{3\,\mathrm{m}^3}^{9\,\mathrm{m}^3} \frac{nRT}{V}\,\mathrm{d}V`
   | **Hinweis:** :math:`\ln{x} - \ln{y} = \ln{\frac{x}{y}}`
c) :math:`\int_4^9 \int_0^x \frac{n \sqrt{x}}{T \sqrt{f}}\,\mathrm{d}f 
   \mathrm{d}x`
d) :math:`\int_0^\pi u(k)\,\mathrm{d}k` mit :math:`u(k) = -\cos{k}`
e) :math:`\int_0^x (t^2 - t - 1)\,\mathrm{d}t`
f) :math:`\int_0^e \frac{x^2 + 2x + 3}{2x}\,\mathrm{d}x`
g) :math:`\frac{\mathrm{d}z(\theta)}{\mathrm{d}\theta}` mit 
   :math:`z(\theta) = \theta \cdot \ln{(2 \cdot \theta + c)}`
h) :math:`\frac{\mathrm{d}^2 f(x)}{\mathrm{d}x^2}` mit :math:`f(x) = 8 
   \cdot x^{2.5}`
i) :math:`\frac{\mathrm{d}g(y)}{\mathrm{d}y}` mit :math:`g(y) = 
   \frac{3y}{y^2 + 1}`
j) | :math:`\frac{\mathrm{d}}{\mathrm{d}x} \frac{\mathrm{d}}{\mathrm{d}y} f(x,y)`
   | mit :math:`f(x,y) = y \cdot \ln{x} + y^2 x^2 + x + y + z`
k) | :math:`\frac{\mathrm{d}}{\mathrm{d}y} \frac{\mathrm{d}}{\mathrm{d}x} f(x,y)`
   | mit :math:`f(x,y) = y \cdot \ln{x} + y^2 x^2 + x + y + z`

2. Differentialgleichungen
^^^^^^^^^^^^^^^^^^^^^^^^^^

Lösen Sie die folgenden Differentialgleichungen:

a) Das Beispiel oben gilt für konstante Geschwindigkeit :math:`v = 
   \mathrm{const.}`. Verwenden Sie nun eine konstante Beschleunigung: 
   :math:`v(t) = a \cdot t + v_0`. Die Bewegungsgleichung lautet immer noch 
   :math:`\frac{\mathrm{d}s}{\mathrm{d}t} = v`, dieses Mal aber mit 
   zeitabhängigem :math:`v(t)`; lösen Sie nach :math:`t_2` auf.
b) Radioaktiver Zerfall: Die Anzahl an nicht zerfallenen Atomen :math:`c` 
   nimmt gemäß der Gleichung :math:`\frac{\mathrm{d}c}{\mathrm{d}t} = -k 
   \cdot c` ab. Die Konstante :math:`k` betrage 
   :math:`10\,\frac{1}{\mathrm{s}}`. Nach wie viel Sekunden sind nur noch 
   10% der ursprünglichen Atome vorhanden? Hinweis: Hier muss die absolute 
   Zahl an Atomen zum Start :math:`c_\mathrm{Start}` nicht bekannt sein.

3. Umgang mit Einheiten
^^^^^^^^^^^^^^^^^^^^^^^

a) In Amerika darf man auf einigen Straßen :math:`85\,\mathrm{mph}` fahren. Wie 
   schnell ist das in :math:`\mathrm{km}/\mathrm{h}` und SI-Einheiten?
b) Wie viel kinetische Energie hat ein Auto (Masse 40 amerikanische 
   Zentner), das mit dieser Geschwindigkeit fährt? Angaben in 
   :math:`\mathrm{kWh}` und in SI-Einheiten.
c) Ist dieses Auto (US-amerikanische Angabe zum Benzinverbrauch: 
   :math:`40\,\mathrm{mpg}` (miles per gallon)) ein "3-Liter-Auto"?

4. Einheitszelle
^^^^^^^^^^^^^^^^

a) Die Einheitszelle von Aluminium hat eine Kantenlänge von :math:`7.652` 
   atomaren Einheiten. Welches Volumen hat die würfelförmige Einheitszelle 
   in atomaren Einheiten und in SI-Einheiten?
b) Jede Einheitszelle enthält vier Aluminiumatome. Welche Stoffmenge 
   Aluminium ist in einem Würfel der Kantenlänge 1 Zoll enthalten?
c) Wie schwer ist dieser Würfel? (Molmasse Aluminium: 
   :math:`26.982\,\mathrm{\frac{g}{mol}}`)
d) Wie groß ist demnach die Dichte von Aluminium?
